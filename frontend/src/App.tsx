import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import "./styles/dicegame.scss";
import M from "materialize-css";
import Learner from "./components/Learner";
import Admin from "./components/Admin";

function App() {
  M.AutoInit();
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Learner />
        </Route>
        <Route path="/Admin">
          <Admin />
        </Route>
        <Route>
          <Redirect to="/" />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
