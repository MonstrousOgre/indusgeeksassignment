import { createContext, useEffect, useReducer } from "react";
import { login } from "../api/admin";
import useCookie from "../hooks/useCookie";
import authReducer, { Action, stateType } from "../reducers/authReducer";

const initialState: stateType = {
  init: false,
  loggedIn: false,
};

export const LearnerContext = createContext<{ state: stateType; dispatch: React.Dispatch<Action> }>({
  state: initialState,
  dispatch: () => null,
});

export const LearnerProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  const [getCookie, setCookie] = useCookie();

  useEffect(() => {
    let cookie = getCookie("learner");
    if (cookie != "") {
      dispatch({ type: "login", payload: { success: true, cookie: cookie } });
    } else dispatch({ type: "logout" });
  }, []);

  return <LearnerContext.Provider value={{ state, dispatch }}>{children}</LearnerContext.Provider>;
};
