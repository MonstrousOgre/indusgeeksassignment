import { createContext, useEffect, useReducer } from "react";
import { login } from "../api/admin";
import useCookie from "../hooks/useCookie";
import authReducer, { Action, stateType } from "../reducers/authReducer";

const initialState: stateType = {
  init: false,
  loggedIn: false,
};

export const AdminContext = createContext<{ state: stateType; dispatch: React.Dispatch<Action> }>({
  state: initialState,
  dispatch: () => null,
});

export const AdminProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  const [getCookie, setCookie] = useCookie();

  useEffect(() => {
    let cookie = getCookie("admin");
    if (cookie != "") {
      dispatch({ type: "login", payload: { success: true, cookie: cookie } });
    } else dispatch({ type: "logout" });
  }, []);

  return <AdminContext.Provider value={{ state, dispatch }}>{children}</AdminContext.Provider>;
};
