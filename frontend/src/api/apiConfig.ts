import { AxiosRequestConfig } from "axios";

export const apiConfig: AxiosRequestConfig = {
  baseURL: `${process.env.REACT_APP_BACKEND_API_BASE_URL}/api/v1`,
  withCredentials: true,
};
