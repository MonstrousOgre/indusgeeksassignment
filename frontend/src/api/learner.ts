import axios from "axios";
//import { payloadType } from "../reducers/authReducer";
import { apiConfig } from "./apiConfig";

export interface loginDetails {
  username: string;
  password: string;
  nickname: string;
}

export const login = async (login: loginDetails): Promise<any> =>
  await axios
    .create(apiConfig)
    .post("learner/auth", login, apiConfig)
    .then((response) => {
      console.log(response.data);
      return response.data;
    });
