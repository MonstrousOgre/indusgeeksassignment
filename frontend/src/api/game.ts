import axios from "axios";
//import { payloadType } from "../reducers/authReducer";
import { apiConfig } from "./apiConfig";

export interface scoreDetails {
  score: number;
  timeTaken: number;
}

export const save = async (score: scoreDetails, gameId?: string): Promise<any> =>
  await axios
    .create(apiConfig)
    .put("learner/game", score, { params: { id: gameId } })
    .then((response) => {
      return response.data;
    });

export const fetchScores = async (): Promise<any> =>
  await axios
    .create(apiConfig)
    .get("admin/scores")
    .then((response) => {
      return response.data;
    });
