const Navbar = (props: React.HtmlHTMLAttributes<HTMLElement>) => {
  return (
    <nav {...props}>
      <div className="nav-wrapper">{props.children}</div>
    </nav>
  );
};
export default Navbar;
