import React from "react";
import MaterialProps, { getClass } from "./MaterialProps";

interface FormProps extends MaterialProps, React.FormHTMLAttributes<HTMLFormElement> {}

export const Form = (props: FormProps) => {
  let className = getClass(props);
  return <form {...props} className={`${className} ${props.className}`}></form>;
};
