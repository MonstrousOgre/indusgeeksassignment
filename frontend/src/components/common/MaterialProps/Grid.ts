type colSize = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | undefined;

type col = {
  s?: colSize;
  m?: colSize;
  l?: colSize;
  xl?: colSize;
};

const getColSizeClass = (col?: col, prefix = "") => {
  let className = "";
  if (col) {
    className += col?.s ? ` ${prefix}s${col?.s}` : "";
    className += col?.m ? ` ${prefix}m${col?.m}` : "";
    className += col?.l ? ` ${prefix}l${col?.l}` : "";
    className += col?.xl ? ` ${prefix}xl${col?.xl}` : "";
  }
  return className;
};

export const getClassName = (grid: Grid) => {
  if (grid.row) {
    return "row";
  }

  let className = "";
  if (grid.col) {
    className += getColSizeClass(grid.col);
    className += getColSizeClass(grid.offset, "offset-");
    className += getColSizeClass(grid.push, "push-");
    className += getColSizeClass(grid.pull, "pull-");
    className = "col " + className;
  }
  return className;
};

export default interface Grid {
  row?: boolean;
  col?: col;
  offset?: col;
  push?: col;
  pull?: col;
}
