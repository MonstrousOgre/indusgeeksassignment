import Color, { getClassName as getColorClass } from "./Color";
import Grid, { getClassName as getGridClass } from "./Grid";
import Waves, { getClassName as getWaveClass } from "./Waves";

export default interface MaterialProps extends Grid, Waves {
  className?: string;
  materialColor?: Color;
  grid?: Grid;
}

export const getClass = (props: MaterialProps) => {
  let className = props.className ?? "";
  className += props.materialColor ? ` ${getColorClass(props.materialColor!)}` : "";
  className += props.grid ? ` ${getGridClass(props.grid!)}` : "";
  className += " " + getGridClass(props);
  className += " " + getWaveClass(props);
  return className;
};
