import MaterialProps, { getClass } from "../MaterialProps";

interface ColProps extends React.HTMLAttributes<HTMLDivElement>, MaterialProps {}

const Col = (props: ColProps) => {
  let className = getClass(props);
  return <div {...props} className={`${className} ${props.className}`}></div>;
};
export default Col;
