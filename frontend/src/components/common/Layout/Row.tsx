import MaterialProps, { getClass } from "../MaterialProps";

interface RowProps extends MaterialProps, React.HTMLAttributes<HTMLDivElement> {}

const Row = (props: RowProps) => {
  let className = getClass(props);
  return <div {...props} className={`row ${className} ${props.className}`}></div>;
};
export default Row;
