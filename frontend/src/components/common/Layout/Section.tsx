import MaterialProps, { getClass } from "../MaterialProps";

interface SectionProps extends MaterialProps, React.HTMLAttributes<HTMLDivElement> {}

const Section = (props: SectionProps) => {
  let className = getClass(props);
  return <div {...props} className={`section ${className} ${props.className}`}></div>;
};
export default Section;
