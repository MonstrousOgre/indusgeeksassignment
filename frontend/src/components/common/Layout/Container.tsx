import MaterialProps, { getClass } from "../MaterialProps";

interface ContainerProps extends MaterialProps, React.HTMLAttributes<HTMLDivElement> {}

const Container = (props: ContainerProps) => {
  let className = getClass(props);
  return <div {...props} className={`container ${className} ${props.className}`}></div>;
};
export default Container;
