import MaterialProps from "./MaterialProps";

interface CardProps extends MaterialProps, React.HTMLAttributes<HTMLDivElement> {}

const Card = (props: CardProps) => {
  return (
    <div {...props} className={`card ${props.className ?? ""}`}>
      {props.children}
    </div>
  );
};

interface ContentProps extends MaterialProps, React.HTMLAttributes<HTMLDivElement> {}

export const CardContent = (props: ContentProps) => {
  return (
    <div {...props} className={`card-content ${props.className ?? ""}`}>
      {props.children}
    </div>
  );
};

interface TitleProps extends MaterialProps, React.HTMLAttributes<HTMLSpanElement> {}

export const CardTitle = (props: TitleProps) => {
  return (
    <span {...props} className={`card-title ${props.className ?? ""}`}>
      {props.children}
    </span>
  );
};

interface ImageProps extends MaterialProps, React.HTMLAttributes<HTMLSpanElement> {}

export const CardImage = (props: ImageProps) => {
  return (
    <span {...props} className={`card-image ${props.className ?? ""}`}>
      {props.children}
    </span>
  );
};

interface ActionProps extends MaterialProps, React.HTMLAttributes<HTMLSpanElement> {}

export const CardAction = (props: ActionProps) => {
  return (
    <span {...props} className={`card-action ${props.className ?? ""}`}>
      {props.children}
    </span>
  );
};

export default Card;
