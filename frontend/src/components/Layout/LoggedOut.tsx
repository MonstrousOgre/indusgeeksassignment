import { Fragment } from "react";
import { NavLink } from "react-router-dom";
import Container from "../common/Layout/Container";
import Navbar from "../common/Navbar";
import BrandLogo from "../common/Navbar/BrandLogo";

type LoggedOutProps = { children?: any };

const LoggedOut = (props: LoggedOutProps) => {
  return (
    <Fragment>
      <div style={{ height: "100vw", width: "100hw", position: "fixed" }} className="logout-bg"></div>
      <Navbar id="loggedOutNavbar">
        <Container>
          <BrandLogo>DiceGame</BrandLogo>
          <ul id="nav-mobile" className="right hide-on-med-and-down black-text">
            <li>
              <NavLink to="/Admin">Admin</NavLink>
            </li>
            <li>
              <NavLink to="/">Learner</NavLink>
            </li>
          </ul>
        </Container>
      </Navbar>
      {props.children}
    </Fragment>
  );
};
export default LoggedOut;
