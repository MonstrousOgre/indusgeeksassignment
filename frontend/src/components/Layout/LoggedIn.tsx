import { Fragment } from "react";
import Container from "../common/Layout/Container";
import Navbar from "../common/Navbar";
import BrandLogo from "../common/Navbar/BrandLogo";

type LoggedInProps = { children?: any; logout: () => void };

const LoggedIn = (props: LoggedInProps) => {
  return (
    <Fragment>
      <div style={{ height: "100vw", width: "100hw", position: "fixed" }}></div>
      <Navbar id="loggedInNavbar">
        <Container>
          <ul id="nav-mobile" className="right hide-on-med-and-down black-text">
            <li>
              <a onClick={props.logout}>Logout</a>
            </li>
          </ul>
        </Container>
      </Navbar>
      <Container className="main">{props.children}</Container>
    </Fragment>
  );
};
export default LoggedIn;
