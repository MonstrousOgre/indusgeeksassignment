import { Fragment } from "react";
import { LearnerContext, LearnerProvider } from "../../context/LearnerContext";
import useCookie from "../../hooks/useCookie";
import { Loader } from "../common/Loader";
import LoggedIn from "../Layout/LoggedIn";
import LoggedOut from "../Layout/LoggedOut";
import DiceGame from "./DiceGame";
import Login from "./Login";

const Learner = () => {
  const [getCookie, setCookie] = useCookie();
  let loggedIn = false;
  return (
    <LearnerProvider>
      <LearnerContext.Consumer>
        {({ state, dispatch }) => {
          const logout = () => {
            setCookie("learner", "", 0);
            dispatch({ type: "logout" });
          };
          return state.init ? (
            state.loggedIn ? (
              <LoggedIn logout={() => logout()}>
                <DiceGame />
              </LoggedIn>
            ) : (
              <LoggedOut>
                <Login />
              </LoggedOut>
            )
          ) : (
            <div style={{ height: "100vh" }} className="valign-wrapper">
              <Loader />
            </div>
          );
        }}
      </LearnerContext.Consumer>
    </LearnerProvider>
  );
};
export default Learner;
