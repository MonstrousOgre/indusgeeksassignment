import React, { Fragment, useEffect, useReducer, useState } from "react";
import { save } from "../../api/game";
import { diceReducer, stateType } from "../../reducers/diceReducer";
import Button from "../common/Button";
import Card, { CardAction, CardContent, CardImage, CardTitle } from "../common/Card";
import Col from "../common/Layout/Col";
import Row from "../common/Layout/Row";
import Section from "../common/Layout/Section";

import dice1 from "./images/dice1.svg";
import dice2 from "./images/dice2.svg";
import dice3 from "./images/dice3.svg";
import dice4 from "./images/dice4.svg";
import dice5 from "./images/dice5.svg";
import dice6 from "./images/dice6.svg";

const DiceGame = () => {
  const numbs = [dice1, dice2, dice3, dice4, dice5, dice6];

  const randomize = () => {
    return Math.floor(Math.random() * 6 + 1);
  };

  const initialState: stateType = {
    round: 1,
    canRoll: true,
    numberOnDice: randomize(),
  };

  const [state, dispatch] = useReducer(diceReducer, initialState);

  useEffect(() => {}, []);

  const saveScore = async (score: number) => {
    await save({ score: score, timeTaken: 5 }, state.gameId).then((res) => {
      dispatch({ type: "newRound", payload: { gameId: res } });
    });
  };

  const onRandomize = () => {
    let score = randomize();
    dispatch({ type: "finishRound", payload: { numberOnDice: score } });

    switch (state.round) {
      case 1:
      case 2:
      case 3:
        setTimeout(() => saveScore(score), 5000);
    }
  };

  return (
    <Section>
      <Card>
        <CardContent>
          {state.round < 4 ? (
            <Fragment>
              <CardTitle className="center">Round {state.round}</CardTitle>
              <Section>
                <Row>
                  <Col col={{ s: 5 }} />
                  <Col col={{ s: 2 }}>
                    <Card>
                      <CardImage>
                        <img src={numbs[state.numberOnDice - 1]} />
                      </CardImage>
                    </Card>
                    <Button
                      style={{ width: "100%" }}
                      onClick={() => {
                        onRandomize();
                      }}
                      disabled={!state.canRoll}>
                      Randomize
                    </Button>
                  </Col>
                </Row>
                <Row></Row>
              </Section>
            </Fragment>
          ) : (
            <CardTitle className="center">Thank you for playing!</CardTitle>
          )}
        </CardContent>
      </Card>
    </Section>
  );
};
export default DiceGame;
