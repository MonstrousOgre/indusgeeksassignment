import { Fragment, useContext, useEffect, useState } from "react";
import { fetchScores } from "../../api/game";
import Button from "../common/Button";
import Card, { CardContent, CardTitle } from "../common/Card";
import Col from "../common/Layout/Col";
import Row from "../common/Layout/Row";
import Section from "../common/Layout/Section";
import { Loader } from "../common/Loader";
import Report from "../common/Report";

interface Entry {
  id: string;
  nickname: string;
  score: Number;
  timeTaken: Number;
}

const Dashboard = () => {
  const [entries, setEntries] = useState<Entry[]>([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    //getAllTransactions(authContext.state.access_token ?? "").then((data) => {
    //setEntries(data);
    //setLoaded(true);
    //});
    fetchScores().then((data) => {
      setEntries(data);
      setLoaded(true);
    });
  }, []);

  return (
    <Section>
      <Card>
        <CardContent>
          <CardTitle className="center">User Info</CardTitle>
          {loaded ? (
            <Fragment>
              <Section>
                <Report>
                  <table>
                    <thead>
                      <tr>
                        <th></th>
                        <th>Nickname</th>
                        <th>Score</th>
                        <th>Time Taken</th>
                      </tr>
                    </thead>

                    <tbody>
                      {entries.map((e, i) => (
                        <tr key={e.id}>
                          <td>{i + 1}</td>
                          <td>{e.nickname}</td>
                          <td>{e.score}</td>
                          <td>{e.timeTaken}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </Report>
              </Section>
            </Fragment>
          ) : (
            <Loader />
          )}
        </CardContent>
      </Card>
    </Section>
  );
};

export default Dashboard;
