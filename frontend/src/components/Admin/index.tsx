import { Fragment } from "react";
import { AdminContext, AdminProvider } from "../../context/AdminContext";
import useCookie from "../../hooks/useCookie";
import { Loader } from "../common/Loader";
import LoggedIn from "../Layout/LoggedIn";
import LoggedOut from "../Layout/LoggedOut";
import Dashboard from "./Dashboard";
import Login from "./Login";

const Admin = () => {
  const [getCookie, setCookie] = useCookie();
  return (
    <AdminProvider>
      <AdminContext.Consumer>
        {({ state, dispatch }) => {
          const logout = () => {
            setCookie("admin", "", 0);
            dispatch({ type: "logout" });
          };
          return state.init ? (
            state.loggedIn ? (
              <LoggedIn logout={() => logout()}>
                <Dashboard />
              </LoggedIn>
            ) : (
              <LoggedOut>
                <Login />
              </LoggedOut>
            )
          ) : (
            <div style={{ height: "100vh" }} className="valign-wrapper">
              <Loader />
            </div>
          );
        }}
      </AdminContext.Consumer>
    </AdminProvider>
  );
};
export default Admin;
