import { Fragment, useContext, useReducer } from "react";
import { login } from "../../api/admin";
import { AdminContext } from "../../context/AdminContext";
import useCookie from "../../hooks/useCookie";
//import useAuth from "../../hooks/useAuth";
import { formReducer } from "../../reducers/formReducer";
import Button from "../common/Button";
import Card from "../common/Card";
import { Form } from "../common/Form";
import Col from "../common/Layout/Col";
import Container from "../common/Layout/Container";
import Row from "../common/Layout/Row";
import TextInput from "../common/TextInput";

const Login = () => {
  const [state, dispatch] = useReducer(formReducer, { username: "", password: "" });

  const [getCookie, setCookie] = useCookie();
  const adminContext = useContext(AdminContext);

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    await login(state).then((res) => {
      if (res == true) {
        let cookie = getCookie("admin");
        adminContext.dispatch({ type: "login", payload: { success: res, cookie: cookie } });
      }
    });
  };

  return (
    <Fragment>
      <div className="center-align">
        <Container style={{ marginTop: "8em" }}>
          <Card
            className=" z-depth-1 logout-secondary-bg"
            style={{ display: "inline-block", padding: "32px 48px 0px 48px" }}>
            <Form col={{ s: 12 }} onSubmit={(e) => onSubmit(e)}>
              <Row>
                <Col col={{ s: 12 }}>Enter your credentials</Col>
              </Row>

              <Row>
                <TextInput
                  col={{ s: 12 }}
                  value={state.username}
                  type="text"
                  name="username"
                  id="username"
                  onChange={(e) => {
                    dispatch({ key: "username", value: e.target.value });
                  }}
                  required
                  validate>
                  Username
                </TextInput>
              </Row>

              <Row>
                <TextInput
                  col={{ s: 12 }}
                  value={state.password}
                  type="password"
                  name="password"
                  id="password"
                  onChange={(e) => {
                    dispatch({ key: "password", value: e.target.value });
                  }}
                  required
                  validate>
                  Password
                </TextInput>
              </Row>

              <br />
              <div className="center-align">
                <Row>
                  <Button name="btn_login" col={{ s: 12 }} large waves wavesLight formNoValidate>
                    Login
                  </Button>
                </Row>
              </div>
            </Form>
          </Card>
        </Container>
      </div>
    </Fragment>
  );
};
export default Login;
