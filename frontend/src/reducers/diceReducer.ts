export interface stateType {
  gameId?: string;
  round: number;
  canRoll: boolean;
  numberOnDice: number;
}

type actionType = "newGame" | "finishRound" | "newRound";

type payloadType = {
  gameId?: string;
  numberOnDice?: number;
};

interface Action {
  type: actionType;
  payload: payloadType;
}

export const diceReducer = (state: stateType, action: Action): stateType => {
  const payload = action.payload;
  const newState = { ...state };
  switch (action.type) {
    case "newGame":
      newState.round = 1;
      newState.gameId = undefined;
      newState.canRoll = true;
      break;
    case "finishRound":
      newState.numberOnDice = payload.numberOnDice!;
      newState.canRoll = false;
      break;
    case "newRound":
      newState.canRoll = true;
      newState.round++;
      if (payload.gameId) newState.gameId = payload.gameId;
  }
  return newState;
};
