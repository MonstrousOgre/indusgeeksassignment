interface pairType {
  key: string;
  value: any;
}

export const formReducer = (state: any, pair: pairType) => {
  return { ...state, [pair.key]: pair.value };
};
