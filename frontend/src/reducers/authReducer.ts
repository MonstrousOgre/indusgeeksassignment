export interface stateType {
  init: boolean;
  loggedIn: boolean;
  sessionToken?: string;
}

type actionType = "login" | "logout";

export type payloadType = {
  success: boolean;
  cookie: string;
  error?: string;
};

export interface Action {
  type: actionType;
  payload?: payloadType;
}

export default function authReducer(state: stateType, action: Action): stateType {
  const payload = action.payload;
  switch (action.type) {
    case "login":
      return {
        init: true,
        loggedIn: payload!.success,
        sessionToken: payload!.cookie,
      };
    case "logout":
      return { init: true, loggedIn: false };
  }
}
