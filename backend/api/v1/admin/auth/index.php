<?php

include_once "../../../../config/headers.php";
header("Access-Control-Allow-Methods: POST");

session_name("admin");
session_start();

$contents = file_get_contents("php://input");

$data = json_decode($contents);

if ($data->username == "admin" && $data->password == "admin123") {
  if (!$_SESSION["status"]) {
    $_SESSION["status"] = "authenticated";
  }
  echo json_encode(true);
} else {
  echo json_encode(false);
}

?>
