<?php

include_once "../../../../config/headers.php";

session_name("admin");
session_start();

include_once "../../../../config/database.php";
include_once "../../../../models/game.php";
include_once "../../../../models/learner.php";

$gameRepo = new Game();
$learnerRepo = new Learner();

if ($_SESSION["status"]) {
  $games = $gameRepo->getAll();

  $learners = $learnerRepo->getIdAndNicknames();

  foreach ($games as $game) {
    $game->nickname = $learners[(string) $game->userId];
    $game->id = (string) $game->_id;
    unset($game->_id);
    unset($game->userId);
  }

  echo json_encode($games);
}

?>
