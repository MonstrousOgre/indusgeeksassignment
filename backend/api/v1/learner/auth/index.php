<?php

include_once "../../../../config/headers.php";
header("Access-Control-Allow-Methods: POST");

session_name("learner");
session_start();

include_once "../../../../config/database.php";
include_once "../../../../models/learner.php";

$contents = file_get_contents("php://input");

$data = json_decode($contents);

$repo = new Learner();

$learner = $repo->getByUsername($data->username);

$success = false;

if ($learner != null) {
  if (password_verify($data->password, $learner->password)) {
    $success = true;
    if (!$_SESSION["user"]) {
      $_SESSION["user"] = $learner->_id;
    }
  }
} else {
  $newLearner = new Learner();
  $newLearner->username = $data->username;
  $newLearner->password = password_hash($data->password, PASSWORD_ARGON2ID);
  $newLearner->nickname = $data->nickname;
  $newLearner->create();
  $success = true;
}

echo json_encode($success);

?>
