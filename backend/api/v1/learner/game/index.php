<?php

include_once "../../../../config/headers.php";
header("Access-Control-Allow-Methods: PUT");

session_name("learner");
session_start();

include_once "../../../../config/database.php";
include_once "../../../../models/learner.php";
include_once "../../../../models/game.php";
include_once "../../../../models/round.php";

$contents = file_get_contents("php://input");

$data = json_decode($contents);

$repo = new Game();

if ($_SESSION["user"]) {
  $gameId = isset($_GET["id"]) ? $_GET["id"] : null;
  $game = new Game();
  $game->score = 0;
  $game->timeTaken = 0;
  $game->roundsPlayed = 0;
  if ($gameId != null) {
    $currGame = $repo->get($gameId);
    $game->_id = $currGame->_id;
    $game->score = $currGame->score;
    $game->timeTaken = $currGame->timeTaken;
    $game->roundsPlayed = $currGame->roundsPlayed;
  }
  $game->userId = new MongoDB\BSON\ObjectId($_SESSION["user"]);
  $game->score += $data->score;
  $game->timeTaken += $data->timeTaken;
  $game->roundsPlayed++;
  if ($gameId == null) {
    $game->create();
  } else {
    $game->update();
  }
  $round = new Round();
  $round->score = $data->score;
  $round->timeTaken = $data->timeTaken;
  $round->roundNumber = $game->roundsPlayed;
  $round->gameId = $game->_id;
  $round->create();
  echo (string) $game->_id;
}

?>
