<?php
include_once "../config/database.php";

class Learner {
  private $collection;
  public $_id;
  public $username;
  public $password;
  public $nickname;

  function __construct() {
    $db = Database::connect();
    $this->collection = $db->selectCollection("learners");
  }

  public function getByUsername($username) {
    $document = $this->collection->findOne(["username" => $username]);
    if ($document) {
      return (object) $document;
    }
    return null;
  }

  public function create() {
    $document = clone $this;
    unset($document->_id);

    $insertOneResult = $this->collection->insertOne($document);
    $this->_id = $insertOneResult->getInsertedId();
  }

  public function getAll() {
    $cursor = $this->collection->find();
    $documents = [];
    foreach ($cursor as $document) {
      array_push($documents, $document);
    }
    return $documents;
  }

  public function getIdAndNicknames() {
    $cursor = $this->collection->find();
    $documents = [];
    foreach ($cursor as $document) {
      $documents[(string) $document->_id] = $document->nickname;
    }
    return $documents;
  }
}

?>
