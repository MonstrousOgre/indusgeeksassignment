<?php
include_once "../config/database.php";

class Game {
  private $collection;
  public $_id;
  public $userId;
  public $score;
  public $timeTaken;
  public $roundsPlayed;

  function __construct() {
    $db = Database::connect();
    $this->collection = $db->selectCollection("games");
  }

  public function get($id) {
    $mongoId = new MongoDB\BSON\ObjectId($id);
    $document = $this->collection->findOne(["_id" => $mongoId]);

    return $document;
  }

  public function getAll() {
    $cursor = $this->collection->find();
    $documents = [];
    foreach ($cursor as $document) {
      array_push($documents, $document);
    }
    return $documents;
  }

  public function getByUser($userId) {
    $mongoId = new MongoDB\BSON\ObjectId($userId);
    $cursor = $this->collection->find(["userId" => $mongoId]);
    $documents = [];
    foreach ($cursor as $document) {
      array_push($documents, $document);
    }
    return $documents;
  }

  public function create() {
    $document = clone $this;
    unset($document->_id);

    $insertOneResult = $this->collection->insertOne($document);
    $this->_id = $insertOneResult->getInsertedId();
  }

  public function update() {
    $updateOneResult = $this->collection->replaceOne(["_id" => $this->_id], $this);
  }
}

?>
