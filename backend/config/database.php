<?php

require_once __DIR__ . "/../vendor/autoload.php";

class Database {
  private static $connString = "";

  static function createClient() {
    $client = null;

    if (self::$connString == "") {
      $client = new MongoDB\Client();
    } else {
      $client = new MongoDB\Client(self::$connString);
    }

    return $client;
  }

  static function connect() {
    $client = self::createClient();
    $db = $client->selectDatabase("diceGame");

    return $db;
  }
}

?>
