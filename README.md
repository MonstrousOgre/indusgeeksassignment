# README #

Do check these points before running the code

* Make sure the correct mongoDB connection string is in `backend/config/database.php`
* Make sure you have the correct backend base url in `frontend/.env`. This should include the protocol, hostname, and port.
